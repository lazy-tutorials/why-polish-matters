extends AnimatableBody2D

@onready var balls: Node2D = $"../Balls"

@export var limits := Vector2(150, 800)

# PID Controller values
@export var ai_settings: Array[Vector4] = [Vector4(10, 0, 0, 100)]
@export var ai_level := 0
@export var Kp := 0.5  # Proportional gain
@export var Ki := 0.1  # Integral gain
@export var Kd := 0.01  # Derivative gain
@export var urgency_factor: float = 1.0  # Default is 1, which means equal importance to time_to_reach and energy_level


var prev_error = 0
var integral = 0
var center := 0

func _ready():
	center = floor((limits.x + limits.y) / 2.0)


func set_ai_reaction(level: int):
	level = clamp(level, 0, ai_settings.size() - 1)
	Kp = ai_settings[level].x / 100.0
	Ki = ai_settings[level].y / 100.0
	Kd = ai_settings[level].z / 100.0
	urgency_factor = ai_settings[level].z


func get_best_ball() -> Ball:
	var best_ball = null
	var shortest_time_to_reach = INF
	
	for ball in balls.get_children():
		# Ignore balls that have passed the paddle or are moving away from the paddle
		if ball.position.x <= (self.position.x - 10) and ball.linear_velocity.x > 0:
			var time_to_reach = max(0, (self.position.x - ball.position.x) / ball.linear_velocity.x)
			
			# Calculate the energy adjustment
			var energy_adjustment = (urgency_factor * (ball.energy_state))
			
			# Apply the adjustment to time_to_reach
			time_to_reach = max(0, time_to_reach - energy_adjustment)
			
			if time_to_reach < shortest_time_to_reach:
				shortest_time_to_reach = time_to_reach
				best_ball = ball
	
	return best_ball


func _physics_process(delta):
	var best_ball = get_best_ball()

	# Calculate the error (difference between ball's Y position and paddle's Y position)
	var error = 0
	if best_ball == null:
		error = center - self.position.y
	else:
		error = best_ball.position.y - self.position.y
		
	# Integral accumulation
	integral += error * delta

	# Derivative (rate of error change)
	var derivative = (error - prev_error) / delta

	# PID controller formula
	var output = Kp * error + Ki * integral + Kd * derivative

	# Move the paddle based on the output of the PID controller
	var target_y = position.y + output
	
	position.y = clamp(target_y, limits.x, limits.y)
	
	# Store this error for the next frame's derivative calculation
	prev_error = error
