extends Control

@onready var ai_paddle := $AiPaddle
@onready var player_health_ui = $PlayerHealth
@onready var ai_health_ui = $AiHealth
@onready var balls = $Balls
@onready var count_down = $CountDown

@onready var new_ball = $Animations/NewBall
@onready var new_ball_timer = $Animations/NewBall_Timer
@onready var restart = $Animations/Restart
@onready var ai_score_tone = $Detection/PlayerEndZone/ScoreTone
@onready var player_score_tone = $Detection/AiEndZone/ScoreTone

@export var start_velocity := 500
@export var max_velocity := 1000
@export var min_velocity := 200
@export var split_velocity := 100
@export var velocity_level_step := 25

@export var starting_health := 100
@export var health_regen := 25
@export var player_health := 100
@export var ai_health := 100

@export var max_balls := 5
var next_ball := "ball_left"

# The level of the current AI.
@export var ai_level := 0
var ball_scene = preload("res://ball.tscn")


func _ready():
	ai_paddle.set_ai_reaction(0)
	update_ui()
	count_down.text = "Round 1"
	restart.play("start_game")


func _on_player_end_zone_body_entered(body: Ball):
	# Player loses one health
	player_health = max(0, player_health - body.energy_state)
	ai_score_tone.play()
	update_ui()
	body.queue_free()  # Remove the ball
	if check_game_over():
		new_ball.play("RESET")
	elif balls.get_child_count() == 1:
		new_ball.play("RESET")
		restart.play("serve_left")


func _on_ai_end_zone_body_entered(body: Ball):
	# AI loses one health
	player_score_tone.play()
	ai_health = max(0, ai_health - body.energy_state)
	update_ui()
	body.queue_free()  # Remove the ball
	if check_game_over():
		new_ball.play("RESET")
	elif balls.get_child_count() == 1:
		new_ball.play("RESET")
		restart.play("serve_right")


func _on_void_body_entered(body):
	# Ball is out of bounds! Destroy, and throw error.
	body.queue_free()  # Remove the ball


# Updates the displayed health values
func update_ui():
	player_health_ui.text = str(player_health)
	ai_health_ui.text = str(ai_health)


# Checks if the game is over and takes any necessary action
func check_game_over() -> bool:
	if player_health <= 0:
		print("Player lost!")
		count_down.text = "You Lose"
		clear_balls()
		return true
	elif ai_health <= 0:
		print("AI lost!")
		count_down.text = "You Win"
		restart.play("next_opponent")
		ai_health = starting_health
		player_health = min(player_health + health_regen, starting_health)
		clear_balls()
		return true
	return false


# Resets the ball position and movement
func serve_ball(direction):
	var ball = ball_scene.instantiate()
	balls.call_deferred("add_child", ball)
	ball.set_velocity((direction + Vector2(0, randf_range(-0.1, 0.1))).normalized() * start_velocity)
	ball.connect("ball_fracture", _on_ball_fracture)


func create_ball(pos: Vector2, vel: Vector2):
	var ball: Ball = ball_scene.instantiate()
	ball.position = pos
	ball.set_velocity(vel)
	ball.connect("ball_fracture", _on_ball_fracture)
	balls.call_deferred("add_child", ball)


func clear_balls():
	for ball in balls.get_children():
		ball.clear()
		new_ball.play("RESET")


func calculate_velocity(current: Vector2, direction: Vector2) -> Vector2:
	# Add extra velocity.
	var new_vel = (current + (direction * split_velocity))
	# Make sure we are within global limits.
	return new_vel.normalized() * clamp(new_vel.length(), min_velocity, max_velocity)


func try_new_ball(animation_name):
	if animation_name != null:
		next_ball = animation_name
	
	await get_tree().process_frame
	
	# We are not currently playing.
	if balls.get_child_count() == 0:
		return
	
	if balls.get_child_count() < max_balls:
		new_ball.play(next_ball)
	else:
		new_ball_timer.start()


func _on_ball_fracture(pos, vel):
	create_ball(pos + (Vector2.UP * 10), calculate_velocity(vel, Vector2.UP))
	create_ball(pos + (Vector2.DOWN * 10), calculate_velocity(vel, Vector2.DOWN))


func _on_restart_animation_finished(anim_name):
	match anim_name:
		"start_game":
			serve_ball(Vector2.RIGHT)
			try_new_ball("ball_left")
		"serve_left":
			serve_ball(Vector2.LEFT)
			try_new_ball("ball_right")
		"serve_right":
			serve_ball(Vector2.RIGHT)
			try_new_ball("ball_left")
		"next_opponent":
			serve_ball(Vector2.RIGHT)
			try_new_ball("ball_left")


func _on_new_ball_animation_finished(anim_name):
	match anim_name:
		"ball_left":
			serve_ball(Vector2.LEFT)
			try_new_ball("ball_right")
		"ball_right":
			serve_ball(Vector2.RIGHT)
			try_new_ball("ball_left")


func _on_new_ball_timer_timeout():
	try_new_ball(null)
