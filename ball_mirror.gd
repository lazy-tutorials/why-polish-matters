extends Node2D

@onready var blip = $Blip
@onready var bloop = $Bloop
@onready var label = $Label
var ball: Ball


func on_blip():
	blip.play()


func on_bloop():
	bloop.play()


func destruct():
	queue_free()


func _process(_delta):
	label.text = str(ball.energy_state)
	position = ball.position
