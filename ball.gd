extends RigidBody2D
class_name Ball

@export var initial_velocity := Vector2(500, 0)
@export var ball_speed := 500
@onready var blip = $Blip
@onready var bloop = $Bloop
@onready var label = $Label
@onready var cool_time = $Cooldown

signal ball_fracture(position: Vector2, speed: Vector2)
signal on_blip
signal on_bloop

var energy_state := 1
var collided := false
var new_velocity := false


func set_velocity(vel):
	new_velocity = true
	initial_velocity = vel
	ball_speed = int(initial_velocity.length())


func cooldown():
	# Check and update energy state here.
	label.text = str(energy_state)
	collided = true
	cool_time.start()


func clear():
	energy_state = 0
	collision_mask = 0
	collision_layer = 2
	$Detect.collision_mask = 0


func _physics_process(_delta):
	if new_velocity:
		linear_velocity = initial_velocity
		new_velocity = false
	else:
		# Normalize the velocity vector and multiply by the desired speed
		linear_velocity = linear_velocity.normalized() * ball_speed


func _on_detect_body_entered(body):
	if body is Ball:
		if energy_state == 10:
			emit_signal("ball_fracture", position, linear_velocity.normalized() * ball_speed)
			bloop.play()
			emit_signal("on_bloop")
			if body.energy_state < 10:
				body.energy_state = 1
			queue_free()
		elif body.energy_state < 10 and !collided:
			blip.play()
			emit_signal("on_blip")
			# Swap values. Only swap once.
			var spd = body.ball_speed
			var energy = body.energy_state
			body.ball_speed = ball_speed
			body.energy_state = energy_state
			ball_speed = spd
			energy_state = energy
			# Don't let the other ball swap if we already swapped.
			body.cooldown()
	else:
		bloop.play()
		emit_signal("on_bloop")
		energy_state = (min(10, energy_state + 1))
	
	cooldown() # Set a short timer to debounce.


func _on_cooldown_timeout():
	collided = false
