extends AnimatableBody2D

# Constants for the screen limits
@export var limits := Vector2(150, 800)


func _process(_delta):
	# Get mouse position
	var mouse_pos = get_viewport().get_mouse_position()
	
	# Determine desired y position
	var target_y = clamp(mouse_pos.y, limits.x, limits.y)
	self.position.y = target_y
