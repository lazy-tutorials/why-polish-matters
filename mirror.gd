extends Node
@onready var player_health := $ClassicGame/SubViewport/Pong/PlayerHealth
@onready var ai_health := $ClassicGame/SubViewport/Pong/AiHealth
@onready var count_down := $ClassicGame/SubViewport/Pong/CountDown
@onready var player_paddle := $ClassicGame/SubViewport/Pong/PlayerPaddle
@onready var ai_paddle := $ClassicGame/SubViewport/Pong/AiPaddle

@onready var mir_player_health = $FancyGame/SubViewport/Pong/PlayerHealth
@onready var mir_ai_health = $FancyGame/SubViewport/Pong/AiHealth
@onready var mir_count_down = $FancyGame/SubViewport/Pong/CountDown
@onready var mir_player_paddle = $FancyGame/SubViewport/Pong/PlayerPaddle
@onready var mir_ai_paddle = $FancyGame/SubViewport/Pong/AiPaddle
@onready var balls = $FancyGame/SubViewport/Pong/Balls
@onready var fancy_game = $FancyGame
var mirror_scene = preload("res://ball_mirror.tscn")


func _process(_delta):
	mir_player_health.text = player_health.text
	mir_ai_health.text = ai_health.text
	mir_count_down.text = count_down.text
	mir_player_paddle.position = player_paddle.position
	mir_ai_paddle.position = ai_paddle.position


func _on_pong_ball_created(ball: Ball):
	var mirror := mirror_scene.instantiate()
	mirror.ball = ball
	ball.connect("on_blip", mirror.on_blip)
	ball.connect("on_bloop", mirror.on_bloop)
	ball.connect("tree_exiting", mirror.destruct)
	balls.call_deferred("add_child", mirror)


func _on_reveal_slider_value_changed(value: float):
	var reveal_size = 1600.0 * value
	fancy_game.size.x = reveal_size
